/*
*	平文をアトバシュ暗号化する
*
* 	@param {string} text
*
*	@return {string}
*/
function convertAtbash(text){
	var convertedText = "";

	for(var i=0;i<text.length;i++){
		var decCh = text[i].charCodeAt(0);

		if(decCh >= DEC_CAP_A && decCh < DEC_CAP_A + 26){//大文字のとき
			decCh = 2 * DEC_CAP_A + 25 - decCh;
		}else if(decCh >= DEC_SML_A && decCh < DEC_SML_A + 26){//小文字のとき
			decCh = 2 * DEC_SML_A + 25 - decCh;
		}

		convertedText += String.fromCharCode(decCh);
	}

	return convertedText;
}


/*
*	平文をシーザー暗号化する
*
* 	@param {string} text
*	@param {int} key
*
*	@return {string}
*/
function convertCaeser(text, c_key){
	var convertedText = "";
	var key = 0;
	if(typeof c_key == "string"){
		key = parseInt(c_key);
	}else{
		key = c_key;
	}
	for(var i=0;i<text.length;i++){
		var decCh = text[i].charCodeAt(0);
		var caseSensitiveNum = 0;

		if(decCh >= DEC_CAP_A && decCh < DEC_CAP_A + 26){//大文字のとき
			caseSensitiveNum = DEC_CAP_A;
		}else if(decCh >= DEC_SML_A && decCh < DEC_SML_A + 26){//小文字のとき
			caseSensitiveNum = DEC_SML_A;
		}

		if(caseSensitiveNum != 0){//text[i]がアルファベットであるとき
			if(key < 0){ key = 26 + key; }
			decCh = decCh + key;
			decCh -= caseSensitiveNum;
			decCh = decCh % 26;			//数値が0~25になるようにする
			decCh += caseSensitiveNum;
		}
		convertedText += String.fromCharCode(decCh);
	}
	return convertedText;
}

/*
*	平文(ptext)を鍵(ktext)を用いてビジュネル暗号化する
*	
*	ktextのアルファベットを一文字ずつ、0から25までの数値に変換して、
*	シーザー暗号の関数へ鍵として渡す
*	(例：鍵の1文字目がCである場合、平文の1文字目を2つずらす)
*	ktextが最後の文字までいった場合、ループする
*
*	isDecryptionがTrueの場合、暗号を平文化する
*
* 	@param {string} ptext 平文
*	@param {string} ktext 鍵テキスト
*	@param {bool} isDecryption
*
*	@return {string}
*/
function convertVigenere(ptext, ktext, isDecryption){
	var convertedText = "";
	var keyCount = 0;

	for(var i=0;i<ptext.length;i++){
		var decKey = ktext[keyCount].charCodeAt(0);
		if(decKey >= DEC_CAP_A && decKey < DEC_CAP_A + 26){//大文字のとき
			decKey = decKey - DEC_CAP_A;
		}else if(decKey >= DEC_SML_A && decKey < DEC_SML_A + 26){//小文字のとき
			decKey = decKey - DEC_SML_A;
		}

		if(isDecryption)
			decKey = -decKey;

		convertedText += convertCaeser(ptext[i], decKey);

		if(ptext[i] != " "){
			keyCount++;
			if(keyCount >= ktext.length)
				keyCount = 0;
		}
	}

	return convertedText;
}
function convertVigenereChar(plainChar, charNumber, isDecryption){
	var convertedText = "";
	var key = changeVigenereKey(charNumber);
	if(isDecryption)
		key = -key;

	convertedText += convertCaeser(plainChar, key);

	return convertedText;
}


/*
*	平文をA1Z26で暗号化する
*
*	一つの単語はハイフンで繋がれ、スペースによって区切られる
*
* 	@param {string} text
*
*	@return {string}
*/
function encodeA1z26(text){
	var convertedText = "";

	for(var i=0;i<text.length;i++){
		//テキスト1文字目ではなく、現在処理する文字と一つ前の文字がスペースでない場合、
		//ハイフンを追加する
		if(i !=0 && text[i].charCodeAt(0) != 32 && text[i-1].charCodeAt(0) != 32){
			convertedText += "-";
		}

		var decCh = text[i].charCodeAt(0);

		if(decCh >= DEC_CAP_A && decCh < DEC_CAP_A + 26){//大文字のとき
			decCh = decCh - 64;
		}else if(decCh >= DEC_SML_A && decCh < DEC_SML_A + 26){//小文字のとき
			decCh = decCh - 96;
		}

		if(decCh == 32){//スペースの場合
			convertedText += " ";
		}else{
			convertedText += decCh;
		}
	}
	return convertedText;
}


/*
*	A1Z26で暗号化されたテキストを復号する
*
*	ハイフンで繋がっている数字を一つの単語として認識し、
*	スペースを単語の区切りとする
*	元の文章に関わらず、すべて大文字で復号される
*
* 	@param {string} text
*
*	@return {string}
*/
function decodeA1z26(text){
	var convertedText = "";

	var asciiStr = text.split(" ");
	for(var y=0;y<asciiStr.length;y++){
		var asciiCh = asciiStr[y].split("-");
		for(var i=0;i<asciiCh.length;i++){
			var decCh = parseInt(asciiCh[i]) + 64;
			convertedText += String.fromCharCode(decCh);
		}

		if(y != asciiStr.length - 1)//文章最後の単語でない場合、単語の区切りにスペースを加える
			convertedText += " ";
	}
	return convertedText;
}

/*
*	平文を二進数に変換する
*
*	ASCII文字を10進数に変換し、それを2進数に変換する
*	変換した1文字が8桁に満たない場合、8桁になるように先頭に"0"を追加する
*
* 	@param {string} text
*
*	@return {string}
*/
function encodeBinary(text){
	var convertedText = "";
	
	for(var i=0;i<text.length;i++){
		var binary = text[i].charCodeAt(0).toString(2);

		var zero = "";
		for(var y=0;y<8-binary.length; y++){
			zero += "0";
		}
		binary = zero + binary;
		
		convertedText += binary;
	}
	return convertedText;
}


/*
*	二進数で書かれたテキストをアルファベットへ変換する
*
*	2進数から10進数へ変換し、対応するASCII文字コードに変換する
*
* 	@param {string} text
*
*	@return {string}
*/
function decodeBinary(text){
	var convertedText = "";
	var numOfChar = text.length / 8;

	for(var i=0;i<numOfChar;i++){
		var binCh = text.substr(0,8);
		text = text.substr(8);
		convertedText += String.fromCharCode(parseInt(binCh,2));
	}
	return convertedText;
}