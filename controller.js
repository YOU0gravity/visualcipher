const ANIMATION_SPEED = 500;
var alphabetHistory = new Array();
//'A'と'a'をそれぞれ10進数に変換した数値
const DEC_CAP_A = 65;
const DEC_SML_A = 97;

//playInfo[0]:{int} 現在のアニメーションの状態(0:再生中, 1:一時停止中, 2:停止中)
//playInfo[1]:{string} 一時停止中の暗号アニメーションの名前を格納(例："atbash")
var playInfo = new Array(2,"");
var c_key = 0;
var v_key = 0;
var v_key_str = "";
var beforeRow = -1;
var v_count = 0;

//test
var processingString = "";
var processedString = "";
var textCount = 0;
var rowHistory = -1;

//後で使う
// $(document).on("click", "#play", function(){
// });

//クリックしたボタンに応じた暗号のセットを表示する
$(function() {
	$("#btn_atbash").click(function(){
		hideAndShow("#atbash");
		createAtbashAnimationArea();
	});
	$("#btn_caeser").click(function(){
		hideAndShow("#caeser");
		createCaeserAnimationArea();
	});
	$("#btn_vigenere").click(function(){
		hideAndShow("#vigenere");
		createVigenereAnimationArea();
	});
	$("#btn_a1z26").click(function(){
		hideAndShow("#a1z26");});
	$("#btn_binary").click(function(){
		hideAndShow("#binary");});

	function hideAndShow(id){
		$(".one_cipher").hide();
		$(id).show();
	}
});
//シーザー暗号の鍵を入力すると、即座に表を変更する
$(function(){
	$("#caeser_key").bind('keyup', function(){
		var caeserKey = $("#caeser_key").val();
		setTextAndKey("caeser");
		createCaeserAnimationArea();
	});
});
//ビジュネル暗号の鍵を入力すると、即座に表を変更する
$(function(){
	$("#vigenere_key").bind('keyup', function(){
		// var key = $("#vigenere_key").val();
		// changeVigenereKey(key);
		createVigenereAnimationArea();
	});
});

/*
*	再生・一時停止・停止の状態を変化させ、
*	状態に応じてアニメーションを行う関数を呼び出す
*
*	@param {string} cipher
*	@param {bool} isPause
*
*/
function changePlayState(cipher,isPause){
	if(!isPause){
		if(playInfo[0] == 2){		//停止→再生
			playInfo[0] = 0;
		}else{						//再生or一時停止→停止
			cipher = "";
			playInfo[0] = 2;
			textCount = 0;
		}

		playInfo[1] = cipher;
		setTextAndKey(cipher);
		proceedToNextCharacter(cipher);
	}else{
		if(playInfo[0] == 0){		//再生→一時停止
			playInfo[0] = 1;
		}else if(playInfo[0] == 1){	//一時停止→再生
			playInfo[0] = 0;
			proceedToNextCharacter(playInfo[1]);
		}
	}

	changePlayButton(isPause);
}
/*
*	引数で示された暗号の種類に応じたフォームから、
*	変換前のテキストをアニメーションエリアにセットする
*
*	@param {string} cipher
*/

function setTextAndKey(cipher){
	var text = "";
	switch(cipher){
		case "atbash":
			text = document.getElementById("atbash_text").value;
			break;

		case "caeser":
			text = document.getElementById("caeser_text").value;
			
			var keyStr = document.getElementById("caeser_key").value;
			//全角数字を半角数字に変換
			if(keyStr != undefined){
				keyStr = keyStr.replace(/[Ａ-Ｚａ-ｚ０-９]/g, function(s) {
					return String.fromCharCode(s.charCodeAt(0) - 0xFEE0);
				});
			}
			if(isNaN(keyStr)){
				outputText = "エラー: 鍵に数値を入力してください";
			}else{
				c_key = parseInt(keyStr);
			}

		case "encodeVigenere":
			text = document.getElementById("vigenere_text").value;
			var keyStr = document.getElementById("vigenere_key").value;
			v_key_str = keyStr;
			keyStr = keyStr[0];
			var tempNum = 0;
			if(keyStr != undefined){
				keyStr = keyStr.replace(/[Ａ-Ｚａ-ｚ０-９]/g, function(s) {
					return String.fromCharCode(s.charCodeAt(0) - 0xFEE0);
				});
				tempNum = keyStr.charCodeAt(0);
			}
			if(tempNum >= 65 && tempNum < 91){
				tempNum -= 65;
			}else if(tempNum >= 97 && tempNum < 123){
				tempNum -= 97;
			}else{
				tempNum = 0;
				//エラーを出すようにする
			}
			v_key = tempNum;


			//test
			processingString = text;
			console.log("processingString: " + processingString);
			console.log("v_key: " + v_key);
			console.log("v_key_str: " + v_key_str);
			//v_keyに問題あり
			processedString = convertVigenere(processingString, v_key_str, false);
			console.log("processedString: " + processedString);
		default:
			break;
	}
	$("#strBfrConverted").text(text);
	$("#strAftConverted").text("");
}

/*
*	引数で示された暗号の種類に応じて、
*	次の1文字を暗号化し、
*	charChangeAnimationを呼び出す
*
*	@param {string} cipher
*/
function proceedToNextCharacter(cipher){
	var strBfrConverted = $("#strBfrConverted").text();
	if(strBfrConverted != ""){
		var processingChar = strBfrConverted[0];
		var processedChar = "";
		var strAftConverted = $("#strAftConverted").text();

		switch(cipher){
			case "atbash":
				processedChar = convertAtbash(processingChar);
				break;
			case "caeser":
				processedChar = convertCaeser(processingChar, c_key)
				break;
			case "encodeVigenere":
				processingChar = processingString[textCount];
				processedChar = processedString[textCount];

				var keyChar = v_key_str.charAt(textCount % v_key_str.length);
				var keyNum = getNumberOfAlphabet(keyChar);
				var object = [keyNum, rowHistory];
				emphasizeRow(object);

				rowHistory = keyNum;
				textCount++;
			default :
				break;
		}

		if(cipher != ""){
			var alphabetNumbers = designateAlphabetToEmphasize(processingChar);

			emphasizeColumn(alphabetNumbers);

			charChangeAnimation(processingChar,processedChar, ANIMATION_SPEED);


			//これは最後に
			alphabetHistory[alphabetHistory.length] = processingChar;
		}
	}else{
		console.log("変換できる文字がありません");
	}
}
/*
*	引数のアルファベットと前回処理したアルファベットを、
*	0~25までの数値に変換し、
*	オブジェクトに入れて返す
*
*	@param {string} alphabet
*	@return {string}
*/
function designateAlphabetToEmphasize(alphabet){
	var columnNum = getNumberOfAlphabet(alphabet);

	var beforeColumnNum = -1;
	if(alphabetHistory.length != 0){
		var beforeAlphabet = alphabetHistory[alphabetHistory.length -1];
		beforeColumnNum = getNumberOfAlphabet(beforeAlphabet);
	}

	return new Array(columnNum, beforeColumnNum);
}

//A~Zを0~25に変換して返す
function getNumberOfAlphabet(ch){
	console.log("ch is " + ch);
	var decCh = ch.charCodeAt(0);
	if(decCh >= 65 && decCh < 65 + 26){//大文字のとき
		decCh = decCh - 65;
	}else if(decCh >= 97 && decCh < 97 + 26){//小文字のとき
		decCh = decCh - 97;
	}
	return decCh;
}

/*
*	！！廃止予定！！
*
*	暗号の種類に応じてアニメーションを行う準備をする
*
* 	@param {string} cipher
*
*	@return {void}
*/
function execute(cipher){

	var outputText = null;
	switch(cipher){
		case "caeserAll": 
			var inputText = document.getElementById("caeser_text").value;
			for(var i=0;i<26;i++){
				outputText += "(key:" + i +")";
				outputText += convertCaeser(inputText, i);
				outputText += "\n";
			}
			break;

		case "decodeVigenere":
			var key = document.getElementById("vigenere_key").value;
			var inputText = document.getElementById("vigenere_text").value;
			outputText = convertVigenere(inputText, key, true);
			break;
		case "encodeA1z26":
			var inputText = document.getElementById("a1z26_text").value;
			outputText = encodeA1z26(inputText);
			break;
		case "decodeA1z26":
			var inputText = document.getElementById("a1z26_text").value;
			outputText = decodeA1z26(inputText);
			break;
		case "encodeBinary":
			var inputText = document.getElementById("binary_text").value;
			outputText = encodeBinary(inputText);
			break;
		case "decodeBinary":
			var inputText = document.getElementById("binary_text").value;
			outputText = decodeBinary(inputText);
			break;
		default : 
			setTextAndKey("");
			break;
	}
	document.getElementById("output_text").value = outputText;
}