/*	再生状態に応じてボタンの表示を変化させる
*	@param {boolean} isPause */
function changePlayButton(isPause){
	if(!isPause){

		if(playInfo[0] == 0){
			$(".play").html("■");
			$(".pause-area").html('<button onClick="changePlayState(playInfo[1],true)" class="btn btn-default pause">‖</button>');
		}else{
			$(".play").html("▶︎");
			$(".pause-area").html('');
		}

	}else{

		if(playInfo[0] == 0){
			$(".pause").text("‖");
		}else if(playInfo[0] == 1){
			$(".pause").text("▶︎");
		}

	}
}
/*
*	指定された2文字をアニメーションする
*	(bfrChからaftChに変化し、下にスライドするアニメーション)
*	まだ変換できる文字が残っている場合、
*	proceedToNextCharacterを呼び出す
*
*	@param {string} bfrCh
*	@param {string} aftCh
*	@param {int} time 
*
*/
function charChangeAnimation(bfrCh,aftCh,time){
	var strBfrConverted = $("#strBfrConverted").text().substr(1);
	var strAftConverted = $("#strAftConverted").text() + aftCh;
	if(bfrCh != " "){
		$("#strBfrConverted").text(strBfrConverted);
		$("#animate_char_div").text(bfrCh).css({
			top: 0,
			left: 0,
			opacity: 100,
		});

		//関数が呼び出されてからtime * 1/5経ったときに実行
		setTimeout(function(){
			$("#animate_char_div").text(aftCh);
		}, time/5);

		// time*2/5
		setTimeout(function(){
			//time*2/5 ~ time*4/5
			$("#animate_char_div").css({
				// backgroundColor:"red",
			}).animate({
				top: "30%",
				opacity: 0,
			}, time*2/5);

		},time*2/5);

		//time*4/5
		setTimeout(function(){
			$("#strAftConverted").text(strAftConverted);
		}, time*4/5);

		//time*5/5
		setTimeout(function(){
			if(playInfo[0] == 0){//再生状態
				if($("#strBfrConverted").text()!=""){//まだ変換できる文字がある
					proceedToNextCharacter(playInfo[1]);
				}else{
					changePlayState("",false);
					$("#strAftConverted").text(strAftConverted);
				}
			}
		}, time);
	}else{										//brfChがスペースの場合、アニメーションせず次へ
		if($("#strBfrConverted").text()!=""){
			$("#strBfrConverted").text(strBfrConverted);
			$("#strAftConverted").text(strAftConverted);
			proceedToNextCharacter(playInfo[1]);
		}else{
			changePlayState("",false);
			$("#strAftConverted").text(strAftConverted);
		}
	}
}

/*
*	アトバシュ暗号をアニメーションするためのエリアを動的に生成する
*/
function createAtbashAnimationArea(){
	var html = "";
	html += "<h1 id='strBfrConverted'></h1>";
	$("#sbc_div").html(html);

	html = "";
	html += "<table>";
	html += "<tr>";
	for(var i=0;i<26;i++){
		var tempChar = String.fromCharCode(65 + i);
		html += "<td class='t" + i  + "'>" + tempChar + "</td>";
	}
	html += "</tr><tr>";
	for(var i=0;i<26;i++){
		var tempChar = String.fromCharCode(90 - i);
		html += "<td class='t" + i +"'>" + tempChar + "</td>";
	}
	html += "</tr>";
	html += "</table>";
	$("#table_div").html(html);


	html = "";
	html += "<h1 id='strAftConverted'></h1>";
	$("#sac_div").html(html);
}

/*
*	シーザー暗号をアニメーションするためのエリアを動的に生成する
*/
function createCaeserAnimationArea(){
	var html = "";
	html += "<h1 id='strBfrConverted'></h1>";
	$("#sbc_div").html(html);

	html = "";
	html += "<table>";
	html += "<tr>";
	for(var i=0;i<26;i++){
		var tempChar = String.fromCharCode(65 + i);
		html += "<td class='t" + i  + "'>" + tempChar + "</td>";
	}
	html += "</tr><tr id='changing-table'>";
	for(var i=0;i<26;i++){
		var offset = 0;
		if(typeof c_key == "number"){
			offset += c_key;
		}
		while(65 + i + offset > 90){
			offset -= 26;
		}
		var tempChar = String.fromCharCode(65 + i + offset);
		html += "<td class='t" + i  +"'>" + tempChar + "</td>";
	}
	html += "</tr>";
	html += "</table>";
	$("#table_div").html(html);


	html = "";
	html += "<h1 id='strAftConverted'></h1>";

	$("#sac_div").html(html);
	$("#changing-table").css({
		display:"none",
	});
	$("#changing-table").fadeIn(700);
}

/*
*	ヴィジュネル暗号をアニメーションするためのエリアを動的に生成する
*/
function createVigenereAnimationArea(){
	var html = "";
	html += "<h1 id='strBfrConverted'></h1>";
	$("#sbc_div").html(html);

	html = "";
	html += "<table>";
	html += "<tr>";
	for(var i=0;i<26;i++){
		var tempChar = String.fromCharCode(65 + i);
		html += "<td class='t" + i  + "'>" + tempChar + "</td>";
	}
	html += "</tr></table><table id='changing-table'>";
	for(var y=0;y<26;y++){
		html += "<tr id='row" + y + "'>";
		for(var i=0;i<26;i++){
			var tempCh = 65 + i + y;
			while(tempCh > 90){
				tempCh -= 26;
			}
			var tempChar = String.fromCharCode(tempCh);
			html += "<td height='2' class='t" + i  +"'></td>";
		}
		html += "</tr>";
	}

	html += "</table>";
	$("#table_div").html(html);


	html = "";
	html += "<h1 id='strAftConverted'></h1>";

	$("#sac_div").html(html);
	for(var i=0;i<26;i++){
		$("#column" + i).css({
			height: "5px",
		});
	}
	// changeVigenereKey(0);
}
// //@param {int} number
// //暗号化する平文を前から数えた数nを受け取って、いくつずらすかを返す(i.e.シーザーの鍵)
// function changeVigenereKey(number){
// 	var keyLength = v_key_str.length;
// 	var tempNum = 0;
// 	if(keyLength > 0){
// 		var keyNumber = number % keyLength;
// 		if(keyNumber <= 0){
// 			keyNumber = 1;
// 		}
// 		var theAlphabet = v_key_str.indexOf(keyNumber - 1);
// 		tempNum = theAlphabet.charCodeAt(0);
// 		if(tempNum >= 65 && tempNum < 91){
// 			tempNum -= 65;
// 		}else if(tempNum >= 97 && tempNum < 123){
// 			tempNum -= 97;
// 		}else{
// 			tempNum = 0;
// 			//エラーを出すようにする
// 		}
		
// 	}
// 	emphasizeRow([tempNum,beforeRow]);
// 	// $("#column" + tempNum).fadeIn(700);
// 	return tempNum;
// }

/*
*	tableの指定した列を強調して表示する
*
*	左から数えてcolumnNum番目の列を強調して表示する
*	呼び出されるのが二回目以降の場合、一つ前に強調された列を元に戻す
*
*	@param {int} columnNum
*	@param {int} beforeColumnNum
*/
function emphasizeColumn(numbers){
	var columnNum = numbers[0];
	var beforeColumnNum = numbers[1];

	var originColor = 'white';
	var originWidth = '28px';
	var changedColor = '#ccffff';
	var changedWidth = '200px';
	$(".t" + columnNum).animate({
		width: changedWidth,
	});
	$(".t" + columnNum).css({
		backgroundColor: changedColor,
	});

	if(beforeColumnNum != -1){
		if(columnNum != beforeColumnNum){ //前の文字が今の文字と違う場合のみ
			$(".t"+ beforeColumnNum).animate({
				width: originWidth,
			});
			$(".t"+ beforeColumnNum).css({
				backgroundColor: originColor,
			});
		}
	}
}
function emphasizeRow(numbers){
	console.log("Row " + numbers[0]);
	console.log("bRow " + numbers[1]);
	var rowNum = numbers[0];
	var beforeRowNum = numbers[1];

	var originColor = 'white';
	var originHeight = '2px';
	var changedColor = '#ccffff';
	var changedHeight = '35px';

	if(beforeRowNum != -1){
		if(rowNum != beforeRowNum){ //前の文字が今の文字と違う場合のみ
			$("#row"+ beforeRowNum).html("");
			$("#row"+ beforeRowNum).animate({
				height: originHeight,
			});
			$("#row"+ beforeRowNum).css({
				backgroundColor: originColor,
			});
		}
	}
	
	var html = "";
	for(var i=0;i<26;i++){
		var tempCh = 65 + i + rowNum;
		while(tempCh > 90){
			tempCh -= 26;
		}
		var tempChar = String.fromCharCode(tempCh);
		html += "<td class='t" + i  +"'>" + tempChar + "</td>";
	}
	$("#row" + rowNum).html(html);

	$("#row" + rowNum).animate({
		height: changedHeight,
	});
	$("#row" + rowNum).css({
		backgroundColor: changedColor,
	});

}